import 'package:flutter/material.dart';
import 'platform_alert.dart';
class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }

}

class _HomeScreenState extends State<StatefulWidget> {
  int appCounter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home screen'),),
      body: Text('Demo dialog of alert.'),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.notifications),
        onPressed: () {
          appCounter++;
          // Show alert
          final alert = PlatformAlert(
            title: 'Times of opened app',
            message: 'Your have tap on the button is $appCounter times.',
          );
          alert.show(context);
        },
      ),
    );
  }


}