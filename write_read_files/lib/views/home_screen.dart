import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }

}

class _HomeScreenState extends State<StatefulWidget> {
  String documentsPath = '';
  String tempPath = '';
  late File myFile;
  String fileText = '';


  @override
  void initState() {
    getPaths().then((value) {
      myFile = File('$documentsPath/pizzas.txt');
      writeFile();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Read/Write files.'),),
      body: Column(
        children: [
          Text('Doc path: ' + documentsPath),
          Text('Temp path' + tempPath),
          ElevatedButton(
            child: Text('Read File'),
            onPressed: () => readFile(),
          ),
          Text(fileText),
        ],
      )
    );
  }

  Future<bool> writeFile() async {
    try {
      await myFile.writeAsString('Margherita, Capricciosa, Napoli');
      return true;
    } catch (e) {
      return false;
    }
  }

  Future getPaths() async {
    final docDir = await getApplicationDocumentsDirectory();
    final tempDir = await getTemporaryDirectory();
    setState(() {
      documentsPath = docDir.path;
      tempPath = tempDir.path;
    });
  }

  Future<bool> readFile() async {
    try {
      // Read the file.
      String fileContent = await myFile.readAsString();
      setState(() {
        fileText = fileContent;
      });
      return true;
    } catch (e) {
      // On error, return false.
      return false;
    }
  }
}