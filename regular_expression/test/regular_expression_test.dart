import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Parse JSESSIONID', () {
    var data = 'JSESSIONID=aaf03b84-c10a-4377-a3fa-4ec011dcd7da.LAPTOP-I6R2E4C4; Path=/; HttpOnly; SameSite=Lax';

    RegExp exp = RegExp(r"JSESSIONID=([\w-.]+);");
    var matcher = exp.firstMatch(data);

    var value = matcher?.group(1);

    expect(value, 'aaf03b84-c10a-4377-a3fa-4ec011dcd7da.LAPTOP-I6R2E4C4');

  });
}