import 'package:flutter/material.dart';

class StatefulScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StatefulScreenState();
  }


}

class StatefulScreenState extends State<StatefulWidget> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    setState(() {
      counter++;
    });

    return Center(
      child: Text('StatefulWidget: counter=$counter'),
    );
  }
}