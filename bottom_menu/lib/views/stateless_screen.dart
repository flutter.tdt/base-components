import 'package:flutter/material.dart';

class StatelessScreen extends StatelessWidget {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    counter++;

    return Center(
      child: Column(
        children: [
          Text('StatelessWidget: counter = $counter'),
        ],
      ),
    );
  }

}