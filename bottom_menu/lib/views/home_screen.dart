import 'package:flutter/material.dart';
import 'stateful_screen.dart';
import 'stateless_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }

}

class _HomeScreenState extends State<StatefulWidget> {
  int selectedScreenIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home screen'),),
      body: IndexedStack(
        children: [
          StatefulScreen(),
          StatelessScreen(),
        ],
        index: selectedScreenIndex,
      ),
      bottomNavigationBar: _buildBottomMenu(),
    );
  }

  BottomNavigationBar _buildBottomMenu() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.add_shopping_cart),
          label: 'Shopping',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.trending_up),
          label: 'Report',
        ),
      ],
      currentIndex: selectedScreenIndex,
      onTap: _onBottomMenuTap,
    );
  }

  void _onBottomMenuTap(int value) {
    setState(() {
      selectedScreenIndex = value;
    });
  }
}