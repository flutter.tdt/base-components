import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }

}

class _HomeScreenState extends State<StatefulWidget> {
  final storage = FlutterSecureStorage();
  final myKey = 'myPass';
  var textController = TextEditingController();
  String? reloadedText;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home screen'),),
      body: Container(
        child: Column(
          children: [
            TextField(
              controller: textController,
              onEditingComplete: () {
                setState(() {

                });
              },
            ),
            Text('Input text: ${textController.text}'),
            ElevatedButton(
                onPressed: () {
                  writeToSecureStorage(textController.text);
                },
                child: Text('Save')
            ),
            ElevatedButton(
                onPressed: () async {
                  reloadedText = await readFromSecureStorage();
                  setState(() {

                  });
                },
                child: Text('Load')
            ),
            Text('Reloaed text: ${reloadedText}'),
          ],
        ),
      ),
    );
  }

  Future writeToSecureStorage(text) async {
    await storage.write(key: myKey, value: text);
  }

  Future<String?> readFromSecureStorage() async {
    String? secret = await storage.read(key: myKey);
    return secret;
  }
}